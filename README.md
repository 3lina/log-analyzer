# Log Analyzer

Log Analyzer is a Python program which analyzes the content of a log file/directory. It accepts arguments as input and return an output after having performed operations on the given input.

## Installation

Log Analyzer runs in a Docker container so in order to execute the program you need to have Docker installed in your machine.

Please perform the below steps to create the Docker image
1. Download sources or git clone the sources
2. Go to the root of the project and execute the following command

```bash
sudo docker build --tag log_analyzer .
```

## Usage

After creating the Docker image, you can start interacting with the program. 
Please go to the directory where your log file/files/directory are located and run the following command

```bash
sudo docker run --rm -v $PWD:/data log_analyzer \
    [file/files or directory to analyze] \
    --operations [operation1] --operations [operation2] ... \
    --output-file [name of the output file] \
    --output-type [type of the output file]
```

Here is an example

```bash
sudo docker run --rm -v $PWD:/data log_analyzer \
    access.log sample-log.log \
    -o most-frequent-client-ip \
    -o total-bytes-exchanged \
    -f result.json
```

The result JSON file will be
```json
{
    "version": "1.0",
    "timestamp": "2021-11-04 15:28:49.781673",
    "log-analysis": [
        {
            "path": "access.log",
            "most-frequent-client-ip": [
                "127.0.0.1"
            ],
            "total-bytes-exchanged": "3631502923"
        },
        {
            "path": "sample-log.log",
            "most-frequent-client-ip": [
                "10.105.21.199"
            ],
            "total-bytes-exchanged": "47941"
        }
    ]
}
```

For any further information about the command line usage you can call the help operation

```bash
$ sudo docker run --rm log_analyzer --help
Usage: cli.py [OPTIONS] [LOGPATHS]...

Options:
  -o, --operations [most-frequent-client-ip|least-frequent-client-ip|most-frequent-server-ip|least-frequent-server-ip|events-per-second|total-bytes-exchanged]
                                  Operations to apply on log files
  -r, --recursive                 If the path is a directory, retrieve files
                                  recursively
  -f, --output-file PATH          [required]
  -t, --output-type [json]        Format of the output file
  --help                          Show this message and exit.
```

## Evolution

If one wants to add a new operation, or support new input or output types, he can easily edit the corresponding source files. 
