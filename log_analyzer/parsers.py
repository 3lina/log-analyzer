import pandas as pd
from pathlib import Path


def parse_csv(path: Path) -> pd.DataFrame:
    """
    Function that takes a file path in argument and parses it into a dataframe.
    This file must be in a csv format for this function to work.

    """
    df = pd.read_csv(path, delim_whitespace=True, header=None, on_bad_lines='warn', names=[
        'Timestamp', 
        'Response header size',
        'Client ip address',
        'Http response code',
        'Response size in bytes',
        'Http request method',
        'URL',
        'Username',
        'Destination IP address',
        'Response type'
    ])
    return df


# This mapping is used to call the correct function according to the input file format
# It allows to easily add new file format and their parsing functions. 
# For example, if we want a json format, then we will add a new function 
# specifically designed for parsing json format to dataframe.
PARSERS = {
    "log": parse_csv,
    "txt": parse_csv,
    "csv": parse_csv
}