import json
from pathlib import Path


def to_json(data: dict, path: Path):
    """
    Function that takes data dictionary in argument, parses it into a json file and saves it into a file located
    in path.
    This file must be in a csv format for this function to work.
    """
    with path.open("w") as file:
        json.dump(data, file, indent=4)


# This mapping is used to call the correct function according to the output file format
# It allows to easily add new file format and their parsing functions. 
# For example, if we want a csv output format, then we will add a new function 
# specifically designed for parsing output format to dataframe.
WRITERS = {
    "json": to_json
}
