import pandas as pd
from typing import List, Dict


def most_frequent_client_ip_address(df: pd.DataFrame) -> List[str]:
    """
    Function that takes a dataframe in parameters and returns the most frequent client ip address in the log file.
    If there are numerous ip addresses, the function will return a list of ip addresses.
    """

    # counts the number of occurrences for each client ip address in the log file
    count_occ_client_ip = df['Client ip address'].value_counts() 

    # retrieves the highest number of occurrences
    max_occ_value = count_occ_client_ip.max() 

    # returns the ip address (or addresses if there are various) with the highest number of occurrences
    # and its value
    most_frequent_client_ip = count_occ_client_ip.where(count_occ_client_ip == max_occ_value)

    # drops the empty lines, selects only the ip address (or addresses) and casts it into a list of string
    most_frequent_client_ip = most_frequent_client_ip.dropna().index.to_list()

    return most_frequent_client_ip


def least_frequent_client_ip_address(df: pd.DataFrame) -> List[str]:
    """
    Function that takes a dataframe in parameters and returns the least frequent client ip address in the log file.
    If there are numerous ip addresses, the function will return a list of ip addresses.
    """

    # counts the number of occurrences for each client ip address in the log file
    count_occ_client_ip = df['Client ip address'].value_counts() 

    # retrieves the lowest number of occurrences
    min_occ_value = count_occ_client_ip.min() 

    # returns the ip address (or addresses if there are various) with the lowest number of occurrences
    # and its value
    least_frequent_client_ip = count_occ_client_ip.where(count_occ_client_ip == min_occ_value)
    
    # drops the empty lines, selects only the ip address (or addresses) and casts it into a list of string
    least_frequent_client_ip = least_frequent_client_ip.dropna().index.to_list() 
    
    return least_frequent_client_ip


def most_frequent_server_ip_address(df: pd.DataFrame) -> List[str]:
    """
    Function that takes a dataframe in parameters and returns the most frequent server ip address in the log file.
    If there are numerous ip addresses, the function will return a list of ip addresses.
    """

    # counts the number of occurrences for each server ip address in the log file
    # drops the lines with no IP address or URL declared (lines with '-')
    count_occ_server_ip = df['Destination IP address'].str.split('/', expand=True).iloc[:,1].value_counts().drop('-')

    # retrieves the highest value of occurrences
    max_occ_value_server = count_occ_server_ip.max()

    # returns the server ip address (or addresses if there are various) with the highest number of occurrences
    # and its value
    most_frequent_server_ip = count_occ_server_ip.where(count_occ_server_ip == max_occ_value_server)

    # drops the empty lines, selects only the ip address (or addresses) and casts it into a list of string
    most_frequent_server_ip = most_frequent_server_ip.dropna().index.to_list() 

    return most_frequent_server_ip


def least_frequent_server_ip_address(df: pd.DataFrame) -> List[str]:
    """
    Function that takes a dataframe in parameters and returns the least frequent server ip address in the log file.
    If there are numerous ip addresses, the function will return a list of ip addresses.
    """

    # counts the number of occurrences for each server ip address in the log file
    # drops the lines with no IP address or URL declared (lines with '-')
    count_occ_server_ip = df['Destination IP address'].str.split('/', expand=True).iloc[:,1].value_counts().drop('-')

    # retrieves the highest value of occurrences
    min_occ_value_server = count_occ_server_ip.min()

    # returns the server ip address (or addresses if there are various) with the highest number of occurrences
    # and its value
    least_frequent_server_ip = count_occ_server_ip.where(count_occ_server_ip == min_occ_value_server)

    # drops the empty lines, selects only the ip address (or addresses) and casts it into a list of string
    least_frequent_server_ip = least_frequent_server_ip.dropna().index.to_list() 

    return least_frequent_server_ip


def number_of_events_per_second(df: pd.DataFrame) -> Dict[int, int]:
    """
    Function that returns the number of events that occured for each second in the log file.
    """

    # This line groups the values by second and counts the number of events that happened during a second
    # I chose not to cast the timestamp into datetime as I considered epoch format more precise,
    # even more if our output will be the input for another analyzer tool
    events_per_second = df['Timestamp'].astype(int, errors='ignore').value_counts()
    
    # This line casts the dataframe into a dict [second -> number of events during this second]
    events_per_second = events_per_second.to_dict()

    return events_per_second


def total_amount_of_bytes_exchanged(df: pd.DataFrame) -> str:
    """
    Function that returns the total amount of bytes exchanged in the access log file.
    """

    # This line creates a new dataframe with the two columns with size in bytes
    bytes_exchanged = df[['Response header size','Response size in bytes']]

    # I saw that some response size in bytes values were equal to -1 so the line below replaces the size in
    # bytes equal to -1 by 0
    bytes_exchanged.replace(-1,0)

    # Sums each row of the new dataframe in order to have the exchanged bytes per event
    sum_per_event = bytes_exchanged.sum(axis=1)

    # Sums all the sums of each row to retrieve the total amount of bytes exchanged in the log file
    total_sum_events = sum_per_event.sum().astype(str)

    return total_sum_events


# This mapping will be used to execute the correct function according to the operations the user inputs
# This mapping allows to easily add new operations
OPERATIONS = {
    "most-frequent-client-ip": most_frequent_client_ip_address,
    "least-frequent-client-ip": least_frequent_client_ip_address,
    "most-frequent-server-ip": most_frequent_server_ip_address,
    "least-frequent-server-ip": most_frequent_server_ip_address,
    "events-per-second": number_of_events_per_second,
    "total-bytes-exchanged": total_amount_of_bytes_exchanged
}