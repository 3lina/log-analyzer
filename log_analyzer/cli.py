import click
import datetime
from operations import OPERATIONS
from parsers import PARSERS
from pathlib import Path
from writers import WRITERS

OUTPUT_VERSION = "1.0"


@click.command()
@click.argument(
    "logpaths",
    nargs=-1,
    type=Path
) 
@click.option(
    "--operations", "-o",
    multiple=True,
    type=click.Choice(OPERATIONS.keys()),
    help="Operations to apply on log files"
)
@click.option(
    "--recursive", "-r", 
    is_flag=True,
    help="If the path is a directory, retrieve files recursively"
)
@click.option(
    "--output-file", "-f",
    type=Path, 
    required=True,
    help="Path of the output file"
)
@click.option(
    "--output-type", "-t",
    default="json",
    type=click.Choice(WRITERS.keys()),
    help="Format of the output file"
)
def main(logpaths, operations, recursive, output_file, output_type):
    logfiles = []

    # This loop retrieves all files in the logpaths entered by the user in the CLI
    # and stores them into an array
    for logpath in logpaths:
        if logpath.is_dir():
            if recursive:
                for ext in PARSERS:
                    logfiles.extend(logpath.rglob("*." + ext))  # stores all the files with {ext} extension in logfiles
            else:
                for ext in PARSERS:
                    logfiles.extend(logpath.glob("*." + ext))  # same as above but recursively
        elif logpath.is_file():
            logfiles.append(logpath)  # appends a file in logfiles

    # The instructions below will convert the logfiles array into a dictionary of dataframes
    # Each dataframe of this array corresponds to a file parsed
    # The first column of the dataframe will be the name of the file and the second column will be this file parsed
    # into a dataframe
    dataframes = {}
    for logfile in logfiles:
        ext = logfile.suffix.replace(".", "")
        dataframes[logfile] = PARSERS[ext](logfile)

    # Creates a dictionary with the result of the operations
    # This dictionary will have the output version, the timestamp when this output was created and the result
    # of the operations on the file in log-analysis
    output_data = {
        "version": OUTPUT_VERSION,
        "timestamp": str(datetime.datetime.now()),
        "log-analysis": []
    }

    # Creates the output dictionary for each file in the dataframe dictionary
    for logfile, dataframe in dataframes.items():
        analysis = {
            "path": str(logfile)  # retrieves and stores the name of the file analyzed
        }
        # For each operation, these instructions create a dictionary with the name of the operation as a key
        # and the result of the operation function as the value
        for operation in operations:
            # execute the operation function according to the mapping in OPERATIONS
            analysis[operation] = OPERATIONS[operation](dataframe)

        output_data["log-analysis"].append(analysis)  # appends the result in log-analysis

    # Write the output dictionary to an output file with the format input by the user in the CLI
    WRITERS[output_type](output_data, output_file)


if __name__ == "__main__":
    main()